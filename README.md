
# MAINTENANCE GUIDE
This project demonstrates how to create a frontend service in [phenex project](https://bitbucket.org/genomicepidemiology/workspace/projects/PHX) and connect it with backend service. We'll use [NTCounter](https://bitbucket.org/genomicepidemiology/ntcounter/src/master/) service as a template service.

#### Requirements

* [git](https://git-scm.com/downloads) installed.
* [docker-compose](https://docs.docker.com/compose/install/) installed.
* You are member of [genomicepidemiology](https://hub.docker.com/orgs/genomicepidemiology) group on docker hub.
* `phenex-ntcounter-frontend` repository created in bitbucket under Phenex project. ![Repository](img/repo.png)

# Architecture

![Refactoring1](img/architecture.png)

# Local Environment
In this section we well set up project on our local machine and tailor it to match our backend service. Finally we will push our frontend & backend service to docker hub.

## Step 1: Clone Frontend Template Service

First let's start with cloning [phenex-resfinder-frontend](https://bitbucket.org/genomicepidemiology/phenex-resfinder-frontend/src/master/) service, and fix url reference so it will point to [phenex-ntcounter-frontend](https://bitbucket.org/genomicepidemiology/phenex-ntcounter-frontend/src/master/) service.

```bash
# Clone service
$ git clone git@bitbucket.org:genomicepidemiology/phenex-resfinder-frontend.git phenex-ntcounter-frontend/ --recursive
$ cd phenex-ntcounter-frontend/
$ git remote -v
$ git remote set-url origin git@bitbucket.org:genomicepidemiology/phenex-ntcounter-frontend.git
$ git remote -v
origin	git@bitbucket.org:genomicepidemiology/phenex-ntcounter-frontend.git (fetch)
origin	git@bitbucket.org:genomicepidemiology/phenex-ntcounter-frontend.git (push)
```

## Step 2: Find & Fix References In Template Service

Because we cloned [phenex-resfinder-frontend](https://bitbucket.org/genomicepidemiology/phenex-resfinder-frontend/src/master/) template service, there is pliantly references in source code to `resfinder` name - **we need to find and fix all of them!**.

```bash
# Find references to resfinder
$ grep -re "resfinder" --exclude-dir=.git .
./config/services/app.nginx.development.conf:  location /services/resfinder/v3 {
./config/services/app.nginx.production.conf:  location /services/resfinder/v3 {
./composer-development.yaml:    image: genomicepidemiology/cge-resfinder-frontend-v3:dev
./composer-development.yaml:    container_name: phenex_dev_resfinder_frontend_v3
./composer-development.yaml:    hostname: resfinder-web-dev
./bitbucket-pipelines.yml:      #       - docker exec phenex_dev_resfinder_frontend-v3 ./tests/tests.sh
./bitbucket-pipelines.yml:            - export IMAGE_NAME=genomicepidemiology/cge-resfinder-v3-frontend:$BITBUCKET_COMMIT
./src/components/Header.vue:                ResFinder source code can be found <a href="https://bitbucket.org/genomicepidemiology/resfinder/src/master/" target="_blank">here</a>.
./src/components/Header.vue:                <li>ResFinder software: <a href="https://bitbucket.org/genomicepidemiology/resfinder.git" target="_blank">3.2 (2020-02-06)</a></li>
./src/components/Header.vue:                <li>ResFinder database: <a href="https://bitbucket.org/genomicepidemiology/resfinder_db.git" target="_blank">2020-02-11</a></li>
```

As you can see from the output, we found interesting context surrounding `resfinder` name. We will replace all `resfinder` references with appropriate counterpart `ntcounter` references.

* `ResFinder`
* `resfinder/v3`
* `resfinder-v3`
* `resfinder-frontend-v3`
* `resfinder_frontend_v3`
* `resfinder`


```bash
{
  grep -rl 'ResFinder' --exclude-dir=.git . | xargs sed -i 's/ResFinder/NTCounter/g';
  grep -rl 'resfinder/v3' --exclude-dir=.git . | xargs sed -i 's/resfinder\/v3/ntcounter\/v1/g';
  grep -rl 'resfinder-v3' --exclude-dir=.git . | xargs sed -i 's/resfinder-v3/ntcounter-v1/g';
  grep -rl 'resfinder-frontend-v3' --exclude-dir=.git . | xargs sed -i 's/resfinder-frontend-v3/ntcounter-frontend-v1/g';
  grep -rl 'resfinder_frontend_v3' --exclude-dir=.git . | xargs sed -i 's/resfinder_frontend_v3/ntcounter_frontend_v1/g';
  grep -rl 'resfinder' --exclude-dir=.git . | xargs sed -i 's/resfinder/ntcounter/g';
}
```

## Step 3: Run project locally

```bash
# Build and run genomicepidemiology/cge-ntcounter-frontend-v1:dev image.
$ docker-compose -f composer-development.yaml build
$ docker-compose -f composer-development.yaml up -d

# Obtain container name
$ docker ps
CONTAINER ID        IMAGE                                               COMMAND                  CREATED             STATUS              PORTS               NAMES
0e359767569b        genomicepidemiology/cge-ntcounter-frontend-v1:dev   "sh -c /app/scripts/…"   24 seconds ago      Up 22 seconds       80/tcp              phenex_dev_ntcounter_frontend_v1

# Obtain IP address of runing container
$ docker inspect phenex_dev_ntcounter_frontend_v1 | grep IPAddress
  "SecondaryIPAddresses": null,
  "IPAddress": "",
          "IPAddress": "172.22.0.2",
```

Once container is running and `npm` finished building frontend application (might take up to 2 minutes) you can access it by container's IP on port `8080` [http://172.22.0.2:8080/](http://172.22.0.2:8080/). Now you should see our service. ![Service](img/service.png)

## Step 4: Tailor New Service

Because every backend service is different, we need to tailor our frontend service to match backend needs. In this step we will do changes to source code manually.

As you can see there is pliantly redundant information that resides after cloning [phenex-resfinder-frontend](https://bitbucket.org/genomicepidemiology/phenex-resfinder-frontend/src/master/) template. E.g. `v3` should be change to `v1` and after looking at [nt-counter](https://bitbucket.org/genomicepidemiology/ntcounter/src/master/) API, arguments that service accepts are `-i`, `-o` so all `OPTIONS` section can be removed.

Open below file(s) with your favorite text editor and remove what's redundant.


### From `src/components/Header.vue`
- Remove HTML content related to `CHANGE LOG` section.
- Remove HTML content related to `REFERENCES` section.
- Remove `div` (and parent div) with `id="changeLog"` attribute.
- Remove `div` (and parent div) with `id="references"` attribute.
- Update description in `id="information"` div.
- Update `a` tag references in `id="software"` div.

After doing those changes you will see service that looks like this. ![Refactoring1](img/refactorin1.png)

### From `src/components/Upload.vue`
- Remove HTML content between `<!-- Start Service Options Section -->` and `<!-- End Service Options Section -->`
- Remove JavaScript leftovers from `serviceOptions` key
- Update value of JavaScript `serviceVersion` key from `v3` to `v1`

Now your service will look like this. ![Refactoring2](img/refactorin2.png)

What's left is to create commit and push changes to [phenex-ntcounter-frontend](https://bitbucket.org/genomicepidemiology/phenex-ntcounter-frontend/src/master/) respository.

```bash
$ git add .
$ git commit -m'renaming resfinder traces'
$ git push origin master
```

## Step 5: Build and Push Image To Docker Hub -manually

In this step we will create production image(s), and we will push them to [docker hub](https://hub.docker.com/orgs/genomicepidemiology). This step requires that you are logged into docker hub - execute `docker login -u USERNAME -p PASSWORD` command on your local machine.

**Frontend App**
```bash
$ docker build -t genomicepidemiology/cge-ntcounter-v1-frontend:1.0 --build-arg DEPLOYMENT=prdouction -f docker/Dockerfile.production .
$ docker push genomicepidemiology/cge-ntcounter-v1-frontend:1.0
```

**Backend App**
```bash
$ cd ..
$ git clone https://ludd@bitbucket.org/genomicepidemiology/ntcounter.git
$ cd ntcounter
$ docker build -t genomicepidemiology/ntc:1.0 -f docker/Dockerfile .
$ docker push genomicepidemiology/ntc:1.0

```
# Production Environment

Production environment is our `nebula cluster`.

## Step 1: Access Kubernees Master Node

This step requires that you have access to all node(s)/cluster(s) mentioned in below instructions, and you know `kube` password.

```bash
# Assess nebula-master node
$ ssh kube@130.226.24.83
```

## Step 2: Locate & Update Phenex Project Repository

Recent years new IT concept came to day light called *Infrastructue as Code* [IaC](https://en.wikipedia.org/wiki/Infrastructure_as_code), in which hardware and software is managed by file(s) describing its state. Phenex project was implemented with IaC in mind. [nebula-provision](https://bitbucket.org/genomicepidemiology/nebula-provision/src/master/) repository describes the latest state of the cluster.  Let's start with locating the repository that hold's state of the kubernetes cluster.

```bash
$ cd /home/kube/nebula-provision
$ git remote -v
$ git status
# On branch master
nothing to commit, working directory clean
$ git pull origin master
$ git stash list
stash@{0}: On master: secrets
```

## Step 3: Provision Service

Next we will located `cge` project and see from which components it's created. At the moment we are interested in all files that contain `resfinder` in name -since resfinder is our template service. As you can see below resfinder service is created from **frontend** and **backend** components. [Frontend](https://bitbucket.org/genomicepidemiology/phenex-ntcounter-frontend/src/master/), component is nothing more than user interface that we created previously, and [backend](https://bitbucket.org/genomicepidemiology/ntcounter/src/master/) is a compute service created by scientist -in this case me :)

```bash
$ cd kubernetes/production/genomicepidemiology/cge/
$ ls -l | grep "resfinder"
-rw-rw-r--. 1 kube kube 6867 Jul  1 13:22 cge.resfinder.v3.backend.cm.yaml
-rw-rw-r--. 1 kube kube 1792 Jul  1 13:26 cge.resfinder.v3.backend.deploy.yaml
-rw-rw-r--. 1 kube kube 1040 Jun 30 22:06 cge.resfinder.v3.frontend.deploy.yaml
-rw-rw-r--. 1 kube kube  456 Jun  1 12:24 cge.resfinder.v3.frontend.svc.yaml
```

### frontend service

Let's start working with frontend service first. The drill is the same, we'll replace `resfinder` with `ntcounter` and `v3` with `v1` and update deployment image to point to public docker [genomicepidemiology/cge-ntcounter-v1-frontend](https://hub.docker.com/repository/docker/genomicepidemiology/cge-ntcounter-v1-frontend) image.

```bash
{
  cp cge.resfinder.v3.frontend.svc.yaml cge.ntcounter.v1.frontend.svc.yaml;
  cp cge.resfinder.v3.frontend.deploy.yaml cge.ntcounter.v1.frontend.deploy.yaml;
  sed -i 's/resfinder-v3/ntcounter-v1/g' cge.ntcounter.v1.frontend.deploy.yaml cge.ntcounter.v1.frontend.svc.yaml;
  sed -i 's/image:.*/image:\ genomicepidemiology\/cge-ntcounter-v1-frontend:1.0/g' cge.ntcounter.v1.frontend.deploy.yaml;
}
```

There is one more file that we need to update to make our service visible edit `cge.ing.yaml` and add bellow lines to `paths:` section at the end of the yaml file.

```yaml
- path: /services/ntcounter/v1
  backend:
    serviceName: cge-ntcounter-v1-frontend
    servicePort: 80
```

### backend service

Next step is to create backend service. The drill is the same, we'll replace `resfinder` with `ntcounter` and `v3` with `v1` and update deployment image to point to public docker [genomicepidemiology/ntc](https://hub.docker.com/repository/docker/genomicepidemiology/ntc) image.

```bash
{
  cp cge.resfinder.v3.backend.deploy.yaml cge.ntcounter.v1.backend.deploy.yaml;
  sed -i 's/resfinder-v3/ntcounter-v1/g' cge.ntcounter.v1.backend.deploy.yaml;
  sed -i 's/resfinder\.v3/ntcounter\.v1/g' cge.ntcounter.v1.backend.deploy.yaml;
  sed -i 's/resfinder/ntcounter/g' cge.ntcounter.v1.backend.deploy.yaml;
  sed -i 's/image:.*/image:\ genomicepidemiology\/ntc:1.0/g' cge.ntcounter.v1.backend.deploy.yaml;
}
```

### queue
The very last thing that connects backend and fronted is **queue**. Without queue you will be able to upload files to server but not analyze them -pretty pointless situation. Information created in frontend service must propagate to backend service (passing by API service serveral times).

To bind backend service with the system, you need to know **Queue** name and **Exchange** name. Which are initialy created and taken from fronted service. **THIS INFORMATION IS TYPE SENSITIVE -SO BETTER TYPE WELL!!!** If you wonder what are those names then have a look at your frontend service.

![](img/queue.png)

Now edit `cge.ntcounter.v1.backend.deploy.yaml` file and update **RABBITMQ_INPUT_*** environment variables by replacing **ResFinder** with **NTCounter** and **v3** with **v1**, so your file after editing will look like one bellow (and save the file).

```yaml
env:
- name: RABBITMQ_INPUT_EXCHANGE
  value: NTCounter
- name: RABBITMQ_INPUT_QUEUE
  value: v1
```

But the queue is not finished yet!. What you did until now you described system as a set of components (boxes) and you set few environment variables to connect to the system. What is missing is to transfered set of options (data) sent from frontend service to match API of [ntc](https://bitbucket.org/genomicepidemiology/ntcounter/src/master/) CLI program. As you can see we have only two options that we care about.

```
-i INPUT_FILE [INPUT_FILE ...], --input-file INPUT_FILE [INPUT_FILE ...] Path to FASTA file(s). If more than one file is provided then use space as separator.
-o OUTPUT_DIR, --output-dir OUTPUT_DIR Path to output directory.
```

### coding the queue

![](img/kill_bill.png)

Copy below `cge.resfinder.v3.backend.cm.yaml` file and replace referenes to resfinder. **Note:** this is the most important file because it bridges CLI program with the system over the messaging queue. **If your program doesn't follow directory structure specified in [ntc](https://bitbucket.org/genomicepidemiology/ntcounter/src/master/) source code, then you will have to make more changes than those described below.**

```bash
{
  cp cge.resfinder.v3.backend.cm.yaml cge.ntcounter.v1.backend.cm.yaml;
  sed -i 's/ResFinder/NTCounter/g' cge.ntcounter.v1.backend.cm.yaml;
  sed -i 's/resfinder\.v3/ntcounter\.v1/g' cge.ntcounter.v1.backend.cm.yaml;
  sed -i 's/v3/v1/g' cge.ntcounter.v1.backend.cm.yaml;
}
```

Next edit `cge.ntcounter.v1.backend.cm.yaml` file and locate below section. Edit it to match API of NTC (yours) program -save the changes.

**Before**

```yaml
# Construct CLI command
command = f'resfinder {args_str} '
command += f'--inputfile {input_files} '
command += f'--outputPath {output_dir} '
command += f'--methodPath {methodPath} '
command += '--databasePath /app/db'
command = command.strip().replace('  ', ' ')
```
and remove

```bash
# Select default method
methodPath = '/bin/kma'
if isFastaFile(msg['files']):
   methodPath = '/bin/blastn'
```

**After**

```yaml
# Construct CLI command
command = f'ntc {args_str} '
command += f'--input-file {input_files} '
command += f'--output-dir {output_dir} '
command = command.strip().replace('  ', ' ')
```

## Step 4: Deploy Service

Deploy new services.
```bash
{
  kubectl apply -f cge.ntcounter.v1.frontend.deploy.yaml;
  kubectl apply -f cge.ntcounter.v1.frontend.svc.yaml;
  kubectl apply -f cge.ntcounter.v1.backend.cm.yaml;
  kubectl apply -f cge.ntcounter.v1.backend.deploy.yaml;
  kubectl apply -f cge.ing.yaml;
}
```
Autoscale new services
```bash
$ kubectl autoscale deployment cge-ntcounter-v1-backend --max=6 --min=3 --cpu-percent=80
$ kubectl autoscale deployment cge-ntcounter-v1-frontend --max=3 --min=1 --cpu-percent=80
$ kubectl -n genomicepidemiology get pods
```
Once deployment and autosceling is seted up. You can access your service at this location [http://genomicepidemiology.io/services/ntcounter/v1/](http://genomicepidemiology.io/services/ntcounter/v1/)



## Step 5: Update server repository

**Please do this step only with production ready services** This is a demo service.

```bash
$ cd /home/kube/nebula-provision/
$ git status
# On branch master
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   cge.ing.yaml
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	cge.ntcounter.v1.backend.cm.yaml
#	cge.ntcounter.v1.backend.deploy.yaml
#	cge.ntcounter.v1.frontend.deploy.yaml
#	cge.ntcounter.v1.frontend.svc.yaml
no changes added to commit (use "git add" and/or "git commit -a")

$ git add .
$ git commit -m'deploing NTCounter service v1'
$ git push origin master
```

## Step 6: Update Phenex frontend services

Very last thing is to add reference to ntc service in [phenex-frontend](https://bitbucket.org/genomicepidemiology/phenex-frontend/src/master/) repository. Please edit [phenex-frontend/src/master/src/components/Navbar.vue](https://bitbucket.org/genomicepidemiology/phenex-frontend/src/master/src/components/Navbar.vue) file, and add below code to `<div class="dropdown-menu" aria-labelledby="navbarDropdown">` div.

```html
<a class="dropdown-item" href="/services/ntcounter/v1">NTCounter v1</a>
```

**Warning:** Be aware that this change will build new `genomicepidemiology/cge-frontend:tage` image and will push it to [docker hub](https://hub.docker.com/). You will have to update this tag in [nebula-provision](https://bitbucket.org/genomicepidemiology/nebula-provision/src/master/) repository `nebula-provision/src/master/kubernetes/production/genomicepidemiology/cge/cge.frontend.deploy.yaml` file and redeploy application.
